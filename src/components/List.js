import '../css/listEntry.css';
const { default: ListEntry } = require("./ListEntry");

const List = (props) => {
    return Array.isArray(props.list) && props.list.length ? (
        <div className="List">
            <ListEntry
                timeStamp="Timestamp"
                from="From"
                to="To"
                value="Value"
                confirmations="Confirmations"
                hash="Hash"
                titles
                clickAddress={props.clickAddress}
            />
            {props.list.map((trans) => {
                return <ListEntry key={trans.hash} { ...trans } clickAddress={props.clickAddress} />;
            })}
        </div>
    ) : null;
}

export default List;
