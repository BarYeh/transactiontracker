import '../css/listEntry.css';
import moment from 'moment';
import { utils } from 'ethers';


const ListEntry = (props) => {
    const convertValue = (val) => {
        const intval = parseInt(val, 10);
        return `${intval / (10**18)} Ether`;
    }
    const convertTimestamp = (val) => {
        const date = moment.unix(parseInt(val, 10));
        return date.format('LLL');
    }
    const getChecksumAddress = address => utils.isAddress(address) ? utils.getAddress(address) : address;
    const checksumAddressFrom = getChecksumAddress(props.from);
    const checksumAddressTo = getChecksumAddress(props.to);
    return (
        <div className="EntryContainer">
            {props.titles
                ? <div className="portion">{props.timeStamp}</div>
                : <div className="portion Timestamp" title={props.timeStamp}>{convertTimestamp(props.timeStamp)}</div>
            }
            {props.titles ? (
                <div className="portion">{props.from}</div>
            ) : (
                <div
                    className="portion From"
                    onClick={() => props.clickAddress(checksumAddressFrom, 0)}
                >
                    {checksumAddressFrom}
                </div>
            )}
            {props.titles ? (
                <div className="portion Timestamp">{props.to}</div>
            ) : (
                <div
                    className="portion To"
                    onClick={() => props.clickAddress(checksumAddressTo, 0)}
                >
                    {checksumAddressTo}
                </div>
            )}
            {props.titles
                ? <div className="portion">{props.value}</div>
                : <div className="portion Value">{convertValue(props.value)}</div>
            }
            
            <div className="portion Confirmations">{props.confirmations}</div>
            <div className="portion Hash">{props.hash}</div>
        </div>
    );
}

export default ListEntry;