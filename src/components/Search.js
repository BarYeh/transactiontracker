import '../css/mainPage.css';
import React, { useState, useEffect } from 'react';
import Loading from 'react-loading';
import { utils } from 'ethers';

const loaderStyle = {
    color: '#3498db',
    type: "spinningBubbles"
}

const disabledStyle = {
  opacity: 0.3,
  cursor: 'default',
  pointerEvents: 'none'
}

function Search(props) {
  const [address, setAddress] = useState('');
  const { isLoading, onClick, text } = props;

  useEffect(() => {
    setAddress(text);
  }, [text]);

  const validAddress = utils.isAddress(address);
  let checksumAddress = address;
  if (validAddress) {
    checksumAddress = utils.getAddress(address);
  }

  return (
      <div className="SearchContainer">
        <input
          type="string"
          className="SearchInput"
          value={address}
          onChange={(element) => setAddress(element.target.value)}
        />
        <div className="SearchButtonContainer">
          {isLoading ? (
            <Loading { ...loaderStyle } />
          ) : (
            <div
              className="SearchButtonTextContainer"
              onClick={validAddress ? () => onClick(checksumAddress, 0) : null}
              style={validAddress ? {} : disabledStyle}
            >
              Search
            </div>
          )}
        </div>
      </div>
  );
}

export default Search;