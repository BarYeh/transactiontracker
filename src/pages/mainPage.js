import { getTransactionList } from '../APIs/API.js';
import List from '../components/List.js';
import '../css/mainPage.css';
import React, { useState } from 'react';
import Search from '../components/Search.js';

function MainPage () {
  const [isLoading, setLoading] = useState(false);
  const [list, setList] = useState([]);
  const [address, setAddress] = useState('');
  const [page, setPage] = useState(0);

  const doSearch = (_address, _page) => {
    setLoading(true);
    setAddress(_address);
    setPage(_page);
    getTransactionList(
      _address.trim(),
      (res) => {
        setList(res.data.result);
        setLoading(false);
      },
      _page + 1
    );
  }

  const _paging = () => {
    const showPrev = page > 0;
    const showNext = list.length === 20;
    const style = {
      height: 20,
      width: 20,
    };
    const dummyExtras = {
      paddingLeft: 10
    }
    return (
      <div className="PagingContainer">
          {showPrev
            ? (
              <div
                className="SearchButtonTextContainer"
                onClick={() => doSearch(address, page - 1)}
                style={style}
              >
                {'<'}
              </div>
            ) : <div style={{ ...style, ...dummyExtras }} />
          }
          {showPrev || showNext ? page + 1 : <div />}
          {showNext
            ? (
              <div
                className="SearchButtonTextContainer"
                onClick={() => doSearch(address, page + 1)}
                style={style}
              >
                {'>'}
              </div>
            ) : <div style={{ ...style, ...dummyExtras }} />
          }
      </div>
    )
  }

  return (
    <div className="App">
      <div className="App-header">
        <div className="Acknowlagement">Powered by Etherscan.io APIs</div>
        <Search
          text={address}
          onClick={doSearch}
          isLoading={isLoading}
        />
        {_paging()}
      </div>
      <List list={list} clickAddress={doSearch} />
    </div>
  );
}

export default MainPage;
