const axios = require('axios');

export const getTransactionList = (address, callback, page, paginate = true) => {
    const baseURL = 'https://api.etherscan.io/api?module=account';
    const action = '&action=txlist';
    const addressVar = `&address=${address}`;
    const pagination = paginate ? `&page=${page}&offset=20` : '';
    const blockLimits = '&startblock=0&endblock=99999999';
    const sortVar = '&sort=asc';
    const APIKey = '&apikey=RQ84WXIJXDH2NJPPY7E439Q81CWFIW5JRG';
    const url = `${baseURL}${action}${addressVar}${pagination}${blockLimits}${sortVar}${APIKey}`;
    axios.get(url)
    .then(res => callback(res))
}