# Transaction Tracker #

Electron project, blockchain explorer

# Set Up #

- To run, clone this repo

- cd to the folder

- run `npm i`

- and then `npm start` (ignore the beast in the browser)

- in a new terminal cd to the folder

- run `npm run electron-dev`

- enjoy!

# Notes #

Nothing very special about programming for electron, mostly invested in doing the design
